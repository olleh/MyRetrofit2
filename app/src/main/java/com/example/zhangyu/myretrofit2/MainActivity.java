package com.example.zhangyu.myretrofit2;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.zhangyu.myretrofit2.Adapter.MyAdapter;
import com.example.zhangyu.myretrofit2.Bean.GankApi;
import com.example.zhangyu.myretrofit2.Bean.RandomData;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Button button;
    private TextView textView;
    private RecyclerView recyclerView;
    private List dataList = new ArrayList();
    private Context context;
    private final String TAG = "MainActivity";
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        initView();

    }

    private void initView() {
        textView = (TextView) findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataRx();
            }
        });
    }

    private void getDataRx() {

        new Retrofit.Builder()
                .baseUrl("https://gank.io/")
                .addConverterFactory(GsonConverterFactory.create())
                //用到Rxjava的时候，需要加入这个
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(GankApi.class)
                .getRandomDataRx("https://gank.io/api/random/data/福利/10")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<RandomData>() {
                    @Override
                    public void accept(RandomData randomData) throws Exception {
                        List<RandomData.ResultsBean> results = randomData.getResults();
                        getResults(results);
                    }
                });

    }

    private void getData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://gank.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GankApi gankApi = retrofit.create(GankApi.class);

        Call<RandomData> data = gankApi.getRandomData("福利", "10");

        data.enqueue(new Callback<RandomData>() {
            @Override
            public void onResponse(Call<RandomData> call, Response<RandomData> response) {
                //textView.setText(response.body().toString());

                List<RandomData.ResultsBean> results = response.body().getResults();
                //Toast.makeText(context, ""+results.toString(), Toast.LENGTH_SHORT).show();
                getResults(results);

            }

            @Override
            public void onFailure(Call<RandomData> call, Throwable t) {

            }
        });
    }

    private void getResults(List<RandomData.ResultsBean> results) {
        Log.e(TAG, " " + results.toString());

        dataList.clear();
        for (RandomData.ResultsBean resultsBean : results) {
            String url = resultsBean.getUrl();
            Log.d(TAG, "onResponse: " + url);
            dataList.add(url);
        }

        recyclerView.setAdapter(new MyAdapter(context, dataList));
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
    }
}
