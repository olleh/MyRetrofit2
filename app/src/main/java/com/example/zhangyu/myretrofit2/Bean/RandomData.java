package com.example.zhangyu.myretrofit2.Bean;

import java.util.List;

public class RandomData {

    /**
     * error : false
     * results : [{"_id":"5b442406421aa92fc4eebe41","createdAt":"2018-07-10T11:12:06.932Z","desc":"创建快速的、动态的recycler views. No adapter, no view holders。","publishedAt":"2018-07-10T00:00:00.0Z","source":"chrome","type":"Android","url":"https://github.com/dev-labs-bg/fast-list","used":true,"who":"lijinshanmx"},{"_id":"58461d71421aa939b58d31e4","createdAt":"2016-12-06T10:07:45.342Z","desc":"Android 翻页效果库","publishedAt":"2016-12-06T11:33:36.433Z","source":"chrome","type":"Android","url":"https://github.com/eschao/android-PageFlip","used":true,"who":"代码家"},{"_id":"56cc6d23421aa95caa707b5a","createdAt":"2015-06-18T00:13:42.711Z","desc":"将语音输入显示为可视化的波纹动画的库","publishedAt":"2015-06-18T03:51:00.21Z","type":"Android","url":"https://github.com/tyorikan/voice-recording-visualizer","used":true,"who":"mthli"},{"_id":"58d92e65421aa969fd8a3dd6","createdAt":"2017-03-27T23:23:17.262Z","desc":"DrawerLayout-like ViewGroup, where a \"drawer\" is hidden under the content view, which can be shifted to make the drawer visible. ","images":["http://img.gank.io/7bf7a986-8ea3-48e2-bb55-5d2e61a7595d"],"publishedAt":"2017-03-28T12:05:55.791Z","source":"web","type":"Android","url":"https://github.com/yarolegovich/SlidingRootNav","used":true,"who":"Yaroslav"},{"_id":"57889f91421aa90df33fe7b3","createdAt":"2016-07-15T16:32:17.0Z","desc":"Android酷炫的礼物卡片控件","publishedAt":"2016-07-18T11:49:19.322Z","source":"web","type":"Android","url":"https://github.com/ldoublem/GiftCard","used":true,"who":"ldoublem"},{"_id":"56cc6d1d421aa95caa7077e7","createdAt":"2015-08-24T14:29:16.354Z","desc":"如何为drawable着色","publishedAt":"2015-08-26T03:51:54.189Z","type":"Android","url":"http://www.jcodecraeer.com/a/anzhuokaifa/androidkaifa/2015/0824/3356.html","used":true,"who":"LHF"},{"_id":"5872f991421aa9315ea79921","createdAt":"2017-01-09T10:46:41.357Z","desc":"Android 横向翻转效果，支持双面的 View 场景","images":["http://img.gank.io/fbc78e71-583f-4d8a-ac55-eb9dbf5f43ce"],"publishedAt":"2017-01-09T11:46:59.821Z","source":"chrome","type":"Android","url":"https://github.com/wajahatkarim3/EasyFlipView","used":true,"who":"代码家"},{"_id":"57497b1967765923298b5caa","createdAt":"2016-05-28T19:03:53.337Z","desc":"图片可拖拽排序的View","publishedAt":"2016-06-07T11:43:18.947Z","source":"chrome","type":"Android","url":"https://github.com/xmuSistone/android-drag-square","used":true,"who":"大熊"},{"_id":"5b18a619421aa9109f56a6be","createdAt":"2018-06-07T11:27:21.905Z","desc":"Flutter实现的闪烁效果。","images":["http://img.gank.io/783d2db8-824e-47a7-9a84-c80e31a2718f"],"publishedAt":"2018-06-07T00:00:00.0Z","source":"chrome","type":"Android","url":"https://github.com/hnvn/flutter_shimmer","used":true,"who":"lijinshanmx"},{"_id":"56dd3a616776592b5d90bb03","createdAt":"2016-03-07T16:22:57.29Z","desc":"全功能颜色选择器","publishedAt":"2016-06-23T11:58:15.971Z","source":"web","type":"Android","url":"https://github.com/AzeeSoft/AndroidPhotoshopColorPicker","used":true,"who":null},{"_id":"5714761167765974f885bf17","createdAt":"2016-04-18T13:52:17.227Z","desc":" 构建 Facebook F8 2016 App / React Native 开发指南","publishedAt":"2016-04-19T12:13:58.869Z","source":"chrome","type":"Android","url":"http://f8-app.liaohuqiu.net/","used":true,"who":"wuzheng"},{"_id":"57ba3c84421aa950d7bc7b60","createdAt":"2016-08-22T07:43:00.364Z","desc":"基于相对位置实现弹出 PopupWindow","images":["http://img.gank.io/ef77f8b5-cc3d-466b-9aee-478ba3396c11"],"publishedAt":"2016-08-22T11:29:37.164Z","source":"chrome","type":"Android","url":"https://github.com/kakajika/RelativePopupWindow","used":true,"who":"代码家"},{"_id":"56cc6d29421aa95caa7081c7","createdAt":"2016-01-20T03:18:03.377Z","desc":"BlockCanary 开源了，轻松找出Android App界面卡顿元凶","publishedAt":"2016-01-20T04:59:02.797Z","type":"Android","url":"https://github.com/moduth/blockcanary","used":true,"who":"mthli"},{"_id":"5a464f9d421aa90fe72536f8","createdAt":"2017-12-29T22:22:21.772Z","desc":"Android通用圆角布局全解析","images":["http://img.gank.io/656dfb2a-c242-4b40-a002-526c4dfd3e24"],"publishedAt":"2018-01-02T08:43:32.216Z","source":"web","type":"Android","url":"http://www.gcssloop.com/gebug/rclayout","used":true,"who":"sloop"},{"_id":"574ec7f5421aa910b3910ae0","createdAt":"2016-06-01T19:33:09.960Z","desc":"Android 用户引导库 MaterialIntroView 使用及源码分析","publishedAt":"2016-06-06T12:24:22.149Z","source":"web","type":"Android","url":"http://www.jianshu.com/p/1d2dcbc1e0f2","used":true,"who":"Abel Joo"},{"_id":"5930e3e5421aa92c73b647a9","createdAt":"2017-06-02T12:04:53.192Z","desc":"基于 Vue 实现的 RecyclerView。","images":["http://img.gank.io/bc4b7f0f-1c79-4041-b16d-03398a9c5f9f"],"publishedAt":"2017-06-02T12:26:37.346Z","source":"chrome","type":"Android","url":"https://github.com/hilongjw/vue-recyclerview","used":true,"who":"代码家"},{"_id":"56cc6d29421aa95caa7082f0","createdAt":"2016-02-19T02:20:55.986Z","desc":"为Gank.io做的一个React-Native客户端","publishedAt":"2016-02-19T03:45:05.174Z","type":"Android","url":"https://github.com/Bob1993/React-Native-Gank","used":true,"who":"MVP"},{"_id":"56cc6d29421aa95caa708313","createdAt":"2016-02-20T03:29:57.863Z","desc":"jlog是一款针对Android开发者的日志工具。","publishedAt":"2016-02-22T04:20:23.514Z","type":"Android","url":"https://github.com/JiongBull/jlog/blob/master/README_ZH.md","used":true,"who":"MVP"},{"_id":"56e9155167765933dbbd2059","createdAt":"2016-03-16T16:12:01.130Z","desc":"一个用于展示注册进度的view ","publishedAt":"2016-03-17T11:14:16.306Z","source":"chrome","type":"Android","url":"https://github.com/jiang111/ProgressView","used":true,"who":"NewTab"},{"_id":"58f774f2421aa954511ebed4","createdAt":"2017-04-19T22:32:18.589Z","desc":"基于 Node.js 的声明式可监控爬虫网络","publishedAt":"2017-04-20T14:03:06.490Z","source":"chrome","type":"Android","url":"https://zhuanlan.zhihu.com/p/26463840","used":true,"who":"王下邀月熊"}]
     */

    private boolean error;
    private List<ResultsBean> results;

    @Override
    public String toString() {
        return "RandomData{" +
                "error=" + error +
                ", results=" + results +
                '}';
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class ResultsBean {
        /**
         * _id : 5b442406421aa92fc4eebe41
         * createdAt : 2018-07-10T11:12:06.932Z
         * desc : 创建快速的、动态的recycler views. No adapter, no view holders。
         * publishedAt : 2018-07-10T00:00:00.0Z
         * source : chrome
         * type : Android
         * url : https://github.com/dev-labs-bg/fast-list
         * used : true
         * who : lijinshanmx
         * images : ["http://img.gank.io/7bf7a986-8ea3-48e2-bb55-5d2e61a7595d"]
         */

        private String _id;
        private String createdAt;
        private String desc;
        private String publishedAt;
        private String source;
        private String type;
        private String url;
        private boolean used;
        private String who;
        private List<String> images;

        @Override
        public String toString() {
            return "ResultsBean{" +
                    "_id='" + _id + '\'' +
                    ", createdAt='" + createdAt + '\'' +
                    ", desc='" + desc + '\'' +
                    ", publishedAt='" + publishedAt + '\'' +
                    ", source='" + source + '\'' +
                    ", type='" + type + '\'' +
                    ", url='" + url + '\'' +
                    ", used=" + used +
                    ", who='" + who + '\'' +
                    ", images=" + images +
                    '}';
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getPublishedAt() {
            return publishedAt;
        }

        public void setPublishedAt(String publishedAt) {
            this.publishedAt = publishedAt;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public boolean isUsed() {
            return used;
        }

        public void setUsed(boolean used) {
            this.used = used;
        }

        public String getWho() {
            return who;
        }

        public void setWho(String who) {
            this.who = who;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }
    }
}
