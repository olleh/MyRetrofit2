package com.example.zhangyu.myretrofit2.Bean;



import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface GankApi {

    /**
     * 数据类型：福利 | Android | iOS | 休息视频 | 拓展资源 | 前端
     * 个数： 数字，大于0
     *
     * @param type
     * @param size
     * @return
     */
    @GET("api/random/data/{type}/{size}")
    Call<RandomData> getRandomData(@Path("type") String type,@Path("size") String size);

    @GET
    Observable<RandomData> getRandomDataRx(@Url String url);

}
