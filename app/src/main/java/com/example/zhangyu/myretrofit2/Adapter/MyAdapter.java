package com.example.zhangyu.myretrofit2.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.zhangyu.myretrofit2.R;

import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    //
    private  Context context;
    private List dataList;

    public MyAdapter(Context context, List list) {
        this.context = context;
        this.dataList = list;
    }

    /**
     * 相当于getView方法中，创建View和ViewHolder
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = View.inflate(context,R.layout.item_recycle,null);
//        return new ViewHolder(itemView);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pic, parent, false);//解决宽度不能铺满
        ViewHolder holder = new ViewHolder(view);
        return holder;

    }


    /**
     * 相当于getView中的数据和View绑定
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //根据位置得到指定数据
        Glide.with(context).load(dataList.get(position)).into(holder.imageView);
    }

    /**
     * 得到总条数
     */
    @Override
    public int getItemCount() {
        return dataList.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//使用getLayoutPosition可以避免添加item后，position的值错误问题
                    Toast.makeText(context, "点击了图标，选择：" + dataList.get(getLayoutPosition()), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}